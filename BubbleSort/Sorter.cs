﻿using System;

#pragma warning disable SA1611

namespace BubbleSort
{
    public static class Sorter
    {
        public static void BubbleSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            if (array.Length == 1)
            {
                return;
            }

            int container = 0;

            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = 0; j < array.Length - 1 - i; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        container = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = container;
                    }
                }
            }
        }

        public static void RecursiveBubbleSort(this int[] array)
        {
            BubbleSort(array);
        }
    }
}
